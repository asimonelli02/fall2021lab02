public class BikeStore {
    public static void main(String[] args) {
        Bicycle[] bike = new Bicycle[4];
        bike[0] = new Bicycle("manu1", 1, 1.0);
        bike[1] = new Bicycle("manu2", 2, 2.0);
        bike[2] = new Bicycle("manu3", 3, 3.0);
        bike[3] = new Bicycle("manu4", 4, 4.0);

        for(int i = 0; i < 4; i++){
            System.out.println(bike[i]);
        }
    }
}
